package command;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Content c = new Content();

        Command insertCommand = new InsertCommand(c);
        insertCommand.doit();
        System.out.println(c.msg); // hello everybody http://www.mashibing.com
        insertCommand.undo();
        System.out.println(c.msg); // hello everybody

        Command copyCommand = new CopyCommand(c);
        copyCommand.doit();
        System.out.println(c.msg); // hello everybody hello everybody
        copyCommand.undo();
        System.out.println(c.msg); // hello everybody

        Command deleteCommand = new DeleteCommand(c);
        deleteCommand.doit();
        System.out.println(c.msg); //  everybody
        deleteCommand.undo();
        System.out.println(c.msg); // hello everybody

        List<Command> commands = new ArrayList<>();
        commands.add(new InsertCommand(c));
        commands.add(new CopyCommand(c));
        commands.add(new DeleteCommand(c));

        for(Command comm : commands) {
            comm.doit();
        }
        System.out.println(c.msg);
        //  everybody http://www.mashibing.comhello everybody http://www.mashibing.com


        for(int i= commands.size()-1; i>=0; i--) {
            commands.get(i).undo();
        }
        System.out.println(c.msg);
        // hello everybody
    }
}