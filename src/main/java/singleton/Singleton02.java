package singleton;

/**
 * 静态内部类方式
 * JVM保证单例
 * 加载外部类时不会加载内部类，这样可以实现懒加载
 */

public class Singleton02 {
    private Singleton02() {
    }

    private static class SingletonHolder {
        private final static Singleton02 INSTANCE = new Singleton02();
    }

    public static Singleton02 getInstance() {
        return SingletonHolder.INSTANCE;
    }

    public void m() {
        System.out.println("m");
    }

    public static void main(String[] args) {
        for(int i=0; i<100; i++) {
            new Thread(()-> System.out.println(Singleton02.getInstance().hashCode())).start();
        }
    }

}
