package template;

public class Demo {
    public static void main(String[] args) {

        Game game = new Cricket();
        game.play();
        System.out.println("----------------"); //分割线
        game = new Football();
        game.play();
    }
}