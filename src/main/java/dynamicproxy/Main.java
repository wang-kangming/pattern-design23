package dynamicproxy;

import java.lang.reflect.Proxy;

public class Main {
    public static void main(String[] args) {
        Tank tank = new Tank();

        // 以下是jdk1.8及其之前的
        System.getProperties().put("sun.misc.ProxyGenerator.saveGeneratedFiles", "true");

        // 以下是jdk1.9及其之后的
        // System.setProperty("jdk.proxy.ProxyGenerator.saveGeneratedFiles", "true");

        Movable m = (Movable) Proxy.newProxyInstance(Tank.class.getClassLoader(),
                new Class[]{Movable.class}, //tank.class.getInterfaces()
                new TimeProxy(tank)
        );

        m.move();
    }
}
