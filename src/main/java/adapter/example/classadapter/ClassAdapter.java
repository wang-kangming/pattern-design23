package adapter.example.classadapter;

// 类适配器类
public class ClassAdapter extends Adaptee implements Target {
    public void request() {
        // some code
        specificRequest();
        // some code
    }
}
