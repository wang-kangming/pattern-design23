package flightweight.example;

public class Client {
    public static void main(String[] args) {
        String sport = "足球";
        for (int i = 1; i <= 5; i++) {
            Gym tyg = GymFactory.getGym(sport);
            tyg.setName("中国体育馆");
            tyg.setShape("圆形");
            tyg.use();
            System.out.println("对象池中对象数量为：" + GymFactory.getSize());
        }
    }
}
