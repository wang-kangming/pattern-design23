package flightweight.example;

import java.util.*;

public class GymFactory {
    private static final Map<String, Gym> gymMap = new HashMap<>();

    public static Gym getGym(String sport) {
        Gym gym = gymMap.get(sport);
        if (gym == null) {
            gym = new Gym(sport);
            gymMap.put(sport, gym);
        }
        return gym;
    }

    public static int getSize() {
        return gymMap.size();
    }
}
