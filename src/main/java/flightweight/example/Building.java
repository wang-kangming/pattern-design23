package flightweight.example;

public interface Building {
    void use();
}
