package prototype.v1;

/**
 * 浅克隆
 */
public class Test {
    public static void main(String[] args) throws Exception {
        Person p1 = new Person();
        Person p2 = (Person)p1.clone();
        System.out.println(p2.age + " " + p2.score); // 8 100
        System.out.println(p2.loc); // Location{street='bj', roomNo=22}

        System.out.println(p1.loc == p2.loc); // true
        p1.loc.street = "sh";
        System.out.println(p2.loc); // Location{street='sh', roomNo=22}
        // 修改的是p1的loc，p2的loc却跟着改变
    }
}

class Person implements Cloneable {
    int age = 8;
    int score = 100;
    Location loc = new Location("bj", 22);

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}

class Location {
    String street;
    int roomNo;

    @Override
    public String toString() {
        return "Location{" +
                "street='" + street + '\'' +
                ", roomNo=" + roomNo +
                '}';
    }

    public Location(String street, int roomNo) {
        this.street = street;
        this.roomNo = roomNo;
    }
}