package state.thread;

public class NewState extends ThreadState_ {
    private Thread_ t;

    public NewState(Thread_ t) {
        this.t = t;
    }

    @Override
    void move(Action input) {
        if ("start".equals(input.msg)) {
            t.state = new RunningState(t);
        }
    }

    @Override
    void run() {

    }
}
