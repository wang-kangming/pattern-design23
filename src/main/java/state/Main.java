package state;

public class Main {
    public static void main(String[] args) {
        MM mm = new MM("LinaTang", new MMHappyState());
        mm.smile();
        mm.cry();

        System.out.println("------");

        mm.setState(new MMSadState());
        mm.smile();
        mm.cry();
    }
}
