package state;


public class MM {
    String name;
    MMState state;

    MM(String name, MMState state) {
        this.name = name;
        this.state = state;
    }

    void setState(MMState state) {
        this.state = state;
    }

    public void smile() {
        state.smile();
    }

    public void cry() {
        state.cry();
    }

    public void say() {
        state.say();
    }
}